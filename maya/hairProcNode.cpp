//-
// ==========================================================================
// Copyright 1995,2006,2008 Autodesk, Inc. All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk
// license agreement provided at the time of installation or download,
// or which otherwise accompanies this software in either electronic
// or hard copy form.
// ==========================================================================
//+

#include "hairProcNode.h"

enum {
        kDrawWireframe,
        kDrawWireframeOnShaded,
        kDrawSmoothShaded,
        kDrawFlatShaded,
        kLastToken
    };
#define LEAD_COLOR              18  // green
#define ACTIVE_COLOR            15  // white
#define ACTIVE_AFFECTED_COLOR   8   // purple
#define DORMANT_COLOR           4   // blue
#define HILITE_COLOR            17  // pale blue

#define RETURN_ON_ERROR(stat, msg) \
if (!stat) \
{ \
    stat.perror((MString(__PRETTY_FUNCTION__)+MString(msg))); \
    return stat; \
}

#define PRINT_ON_ERROR(stat, msg) \
if (!stat) \
{ \
    stat.perror((MString(__PRETTY_FUNCTION__)+MString(msg))); \
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Node implementation with standard viewport draw
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

MObject HairProcNode::_aHairProcPath;
MObject HairProcNode::_aHairFileFn;
MObject HairProcNode::_aDummy;
MObject HairProcNode::_aPreviewQuality;
MObject HairProcNode::_aMinPixelWidth;
MTypeId HairProcNode::id( 0x80007 );

HairProcNode::HairProcNode() : _valid(false), _displayList(0) {}
HairProcNode::~HairProcNode() {}

MStatus HairProcNode::compute( const MPlug& plug, MDataBlock& data )
{
    
    if (plug != _aDummy) return MS::kUnknownParameter;
    //std::cerr << __PRETTY_FUNCTION__ << std::endl;
    MStatus stat;

    MDataHandle hHairFileFn = data.inputValue(_aHairFileFn, &stat);
    RETURN_ON_ERROR(stat, "data.inputValue(_aHairFileFn)");
    std::string hairFileFn = hHairFileFn.asString().asChar();

    MDataHandle hPreviewQuality = data.inputValue(_aPreviewQuality, &stat);
    RETURN_ON_ERROR(stat, "data.inputValue(_aPreviewQuality)");
    int previewQuality = hPreviewQuality.asInt();

    MDataHandle hDummy = data.outputValue(_aDummy, &stat);
    RETURN_ON_ERROR(stat, "data.outputValue(_aDummy)");

    _valid = false;
    _bbmin = _bbmax = MPoint(0,0,0);

    if (hairFileFn == "") return MS::kSuccess;
    _hairFile.Initialize();
    if (_hairFile.LoadFromFile(hairFileFn.c_str()) > 0)
    {
        int hair_count = _hairFile.GetHeader().hair_count;
        int point_count = _hairFile.GetHeader().point_count;
        _bbmin = MPoint(_hairFile.bbmin);
        _bbmax = MPoint(_hairFile.bbmax);
        _valid = true;
        data.setClean(plug);
        std::cout << "loaded hair file " << hairFileFn << std::endl;
        return MS::kSuccess;
    }
    else
    {
        MGlobal::displayError(MString("Could not load hair file: '") + MString(hairFileFn.c_str() + MString("'")));
        return MS::kFailure;
    }
}

void* HairProcNode::getGeometry()
{
    // Force a compute if needed
    //
    MObject thisNode = thisMObject();
    MStatus stat;
    MPlug pDummy(thisNode, _aDummy);
    pDummy.asInt();

    void* ret = NULL;
    if (_valid) ret = &_hairFile;

    return ret;
    
}

bool HairProcNode::isBounded() const
{
    return true;
}

MBoundingBox HairProcNode::boundingBox() const
{
    //std::cerr << __PRETTY_FUNCTION__ << std::endl;
    MObject thisNode = thisMObject();
    MStatus stat;
    MPlug pDummy(thisNode, _aDummy);
    pDummy.asInt();
    MBoundingBox bb;
    bb.expand(_bbmin);
    bb.expand(_bbmax);
    //std::cerr << _bbmin << " " << _bbmax << std::endl;
    return bb;
}

void* HairProcNode::creator()
{
    return new HairProcNode();
}


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Plugin Registration
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

MStatus HairProcNode::initialize()
{
    MFnTypedAttribute tAttr;
    MFnNumericAttribute nAttr;
    MStatus stat;
    _aHairProcPath = tAttr.create("hairProcPath", "hpp", MFnData::kString, &stat);
    tAttr.setStorable(true);
    tAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create hairProcPath");

    _aHairFileFn = tAttr.create("hairFileFn", "hfn", MFnData::kString, &stat);
    tAttr.setStorable(true);
    tAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create hairFileFn");


    _aDummy = nAttr.create("dummy", "d", MFnNumericData::kInt, 0);
    nAttr.setStorable(false);
    nAttr.setWritable(false);
    nAttr.setReadable(true);
    RETURN_ON_ERROR(stat, "create dummy");

    _aPreviewQuality = nAttr.create("previewQuality", "pq", MFnNumericData::kInt, 10);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create previewQuality");

    _aMinPixelWidth = nAttr.create("min_pixel_width", "mpw", MFnNumericData::kFloat, 0.0f);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create min_pixel_width");

    addAttribute(_aHairProcPath);
    addAttribute(_aHairFileFn);
    addAttribute(_aDummy);
    addAttribute(_aPreviewQuality);
    addAttribute(_aMinPixelWidth);

    attributeAffects(_aHairFileFn, _aDummy);
    attributeAffects(_aPreviewQuality, _aDummy);

    return MS::kSuccess;
}


void HairProcNodeUI::draw(const MDrawRequest& request, M3dView& view) const
{
   // std::cerr << __PRETTY_FUNCTION__ << std::endl;
    MDrawData data = request.drawData();
    cyHairFile* hairFile = (cyHairFile*)data.geometry();

    view.beginGL();

        if (hairFile)
        {
            glVertexPointer(3, GL_FLOAT, 0, hairFile->GetPointsArray());
            glEnableClientState(GL_VERTEX_ARRAY);

            int pointIndex = 0;
            int hairCount = hairFile->GetHeader().hair_count;
            const unsigned short* segments = hairFile->GetSegmentsArray();
            if (segments)
            {
                for (int hairIndex = 0; hairIndex < hairCount; hairIndex++)
                {
                    glDrawArrays(GL_LINE_STRIP, pointIndex, segments[hairIndex]+1);
                    pointIndex += segments[hairIndex]+1;
                }
            }
            else
            {
                int dsegs = hairFile->GetHeader().d_segments;
                for (int hairIndex =0; hairIndex < hairCount; hairIndex++)
                {
                    glDrawArrays(GL_LINE_STRIP, pointIndex, dsegs+1);
                    pointIndex+=dsegs+1;
                }
            }

            glDisableClientState(GL_VERTEX_ARRAY);
        }

    view.endGL();
}

void HairProcNodeUI::getDrawRequests(const MDrawInfo& info, bool objectAndActiveOnly, MDrawRequestQueue& queue)
{
    MDrawData data;
    MDrawRequest request = info.getPrototype(*this);
    HairProcNode* hairProcNode = (HairProcNode*)surfaceShape();
    getDrawData(hairProcNode->getGeometry(), data);
    request.setDrawData(data);

    switch (info.displayStyle())
    {
    case M3dView::kWireFrame:
        request.setToken(kDrawWireframe);
        break;
    case M3dView::kGouraudShaded :
        request.setToken(kDrawSmoothShaded);
        break;
    case M3dView::kFlatShaded :
        request.setToken(kDrawFlatShaded);
        break;
    default:
        break;
    }

    M3dView::DisplayStatus displayStatus = info.displayStatus();
    M3dView::ColorTable activeColorTable = M3dView::kActiveColors;
    M3dView::ColorTable dormantColorTable = M3dView::kDormantColors;
    switch ( displayStatus )
    {
        case M3dView::kLead :
            request.setColor( LEAD_COLOR, activeColorTable );
            break;
        case M3dView::kActive :
            request.setColor( ACTIVE_COLOR, activeColorTable );
            break;
        case M3dView::kActiveAffected :
            request.setColor( ACTIVE_AFFECTED_COLOR, activeColorTable );
            break;
        case M3dView::kDormant :
            request.setColor( DORMANT_COLOR, dormantColorTable );
            break;
        case M3dView::kHilite :
            request.setColor( HILITE_COLOR, activeColorTable );
            break;
        default:    
            break;
    }

    queue.add(request);
}

bool HairProcNodeUI::select( MSelectInfo &selectInfo,
                             MSelectionList &selectionList,
                             MPointArray &worldSpaceSelectPts ) const
//
// Select function. Gets called when the bbox for the object is selected.
// This function just selects the object without doing any intersection tests.
//
{
    MSelectionMask priorityMask( MSelectionMask::kSelectObjectsMask );
    MSelectionList item;
    item.add( selectInfo.selectPath() );
    MPoint xformedPt;
    selectInfo.addSelection( item, xformedPt, selectionList,
                             worldSpaceSelectPts, priorityMask, false );
    return true;
}

void* HairProcNodeUI::creator()
{
    //std::cerr << __PRETTY_FUNCTION__ << std::endl;
    return new HairProcNodeUI;
}


void* HairProcNodeTranslator::create()
{
    //std::cerr << __PRETTY_FUNCTION__ << std::endl;
    return new HairProcNodeTranslator;
}

void HairProcNodeTranslator::Export(AtNode* node)
{
    //std::cerr << __PRETTY_FUNCTION__ << std::endl;
    AiNodeSetStr(node, "dso", "/Users/anders/vfx/arnold/4.0.10.2/plugins/aihairproc.dylib");
    AiNodeDeclare(node, "min_pixel_width", "constant FLOAT");
    Update(node);
}

void HairProcNodeTranslator::Update(AtNode* node)
{
    //std::cerr << __PRETTY_FUNCTION__ << std::endl;
    MPlug plug = FindMayaObjectPlug("hairFileFn");
    MString strFn;
    plug.getValue(strFn);
    AiNodeSetStr(node, "dso", "/Users/anders/vfx/arnold/4.0.10.2/plugins/aihairproc.dylib");
    AiNodeSetStr(node, "data", strFn.asChar());
    MFnDagNode fnDagNode(GetMayaDagPath());
    MPoint min = fnDagNode.boundingBox().min();
    MPoint max = fnDagNode.boundingBox().max();
    AiNodeSetPnt(node, "min", min.x, min.y, min.z);
    AiNodeSetPnt(node, "max", max.x, max.y, max.z);

    plug = FindMayaObjectPlug("min_pixel_width");
    AiNodeSetFlt(node, "min_pixel_width", plug.asFloat());

    ExportMatrix(node, 0);

    MPlug shadingGroupPlug = GetNodeShadingGroup(m_dagPath.node(), 0);
    if (!shadingGroupPlug.isNull())
    {
        AtNode *shader = ExportNode(shadingGroupPlug);
        if (shader != NULL)
        {
            AiNodeSetPtr(node, "shader", shader);
        }
    }
}

AtNode* HairProcNodeTranslator::CreateArnoldNodes()
{
    AtNode* ret = NULL;
    if (GetMayaDagPath().isVisible())ret = AddArnoldNode("procedural");
    return ret;
}

extern "C"
{

    void initializeExtension(CExtension& ext)
    {
        MStatus stat;
        MObject obj = CExtensionsManager::GetMayaPlugin();
        MFnPlugin plugin(obj, "AL", "3.0", "Any");
        stat = plugin.registerShape(
                "hairProcNode", 
                HairProcNode::id, 
                HairProcNode::creator, 
                HairProcNode::initialize, 
                HairProcNodeUI::creator
            );
        PRINT_ON_ERROR(stat, "register hairProcNode");
        ext.RegisterTranslator("hairProcNode", "HairProcNodeTranslator", HairProcNodeTranslator::create);
    }

    void deinitializeExtension(CExtension& ext)
    {

    }


}
