#include <iostream>
#include <ai.h>
#include <cstring>
#include "cyHairFile.h"

struct HairProcParams
{
    std::string hairFileFn;
    float min_pixel_width;
};

static int init(AtNode* node, void** user_ptr)
{
    HairProcParams* hp = new HairProcParams;

    hp->hairFileFn = AiNodeGetStr(node, "data");
    hp->min_pixel_width = AiNodeGetFlt(node, "min_pixel_width");

    *user_ptr = hp;

    return TRUE;
}

static int cleanup(void* user_ptr)
{
    return TRUE;
}

static int numNodes(void* user_ptr)
{
    int nn = 1;
    return nn;
}

static AtNode* getNode(void* user_ptr, int i)
{
    AtNode* node = NULL;

    HairProcParams* hp = (HairProcParams*)user_ptr;

    std::cerr << "getNode: " << i << std::endl;
    const char* hairFn = hp->hairFileFn.c_str();
    cyHairFile hairFile;
    if (hairFile.LoadFromFile(hairFn) < 0)
    {
        AiMsgError("[aihairproc] Could not open hair file '%s'", hairFn);
    }
    else
    {
        AiMsgInfo("[aihairproc] hair file '%s'", hairFn);
        int hair_count = hairFile.GetHeader().hair_count;
        int point_count = hairFile.GetHeader().point_count;
        AiMsgInfo("[aihairproc] hair count: %d", hair_count);
        AiMsgInfo("[aihairproc] point count: %d", point_count);
        AiMsgInfo("[aihairproc] bounds are (%f,%f,%f) (%f,%f,%f)", hairFile.bbmin[0], hairFile.bbmin[1], 
                                                                    hairFile.bbmin[2], hairFile.bbmax[0], 
                                                                    hairFile.bbmax[2], hairFile.bbmax[2]);

        unsigned short* segments = hairFile.GetSegmentsArray();
        float* thickness = hairFile.GetThicknessArray();
        AtUInt32* num_points = new AtUInt32[hair_count];
        float* radius = new float[point_count];
        if (segments != NULL)
        {
            AiMsgInfo("[aihairproc] has a segments array");
            for (int i=0; i < hair_count; ++i)
            {
                num_points[i] = segments[i]+1;
            }
        }
        else
        {
            AiMsgInfo("[aihairproc] does not have a segments array");
            for (int i=0; i < hair_count; ++i)
            {
                num_points[i] = hairFile.GetHeader().d_segments;
            }
        }
        if (thickness == NULL)
        {
            AiMsgInfo("[aihairproc] does not have a thickness array");
            for (int i=0; i < point_count; ++i)
            {
                radius[i] = hairFile.GetHeader().d_thickness * 0.5f;
            } 
        }
        else
        {
            AiMsgInfo("[aihairproc] has a thickness array");
            for (int i=0; i < point_count; ++i)
            {
                radius[i] = thickness[i] * 0.5f;
            } 
        }
        
        node = AiNode("curves");
        AtArray* ai_num_points = AiArrayConvert(hair_count, 1, AI_TYPE_UINT, num_points);
        AtArray* ai_points = AiArrayConvert(point_count, 1, AI_TYPE_POINT, hairFile.GetPointsArray());
        AtArray* ai_radius = AiArrayConvert(point_count, 1, AI_TYPE_FLOAT, radius);
        AiNodeSetArray(node, "num_points", ai_num_points);
        AiNodeSetArray(node, "points", ai_points);
        AiNodeSetArray(node, "radius", ai_radius);
        AiNodeSetInt(node, "basis", 5);
        AiNodeSetBool(node, "opaque", false);
        AiNodeSetFlt(node, "min_pixel_width", hp->min_pixel_width);

    }

    
    return node;
}


proc_loader
{
    vtable->Init = init;
    vtable->Cleanup = cleanup;
    vtable->NumNodes = numNodes;
    vtable->GetNode = getNode;
    strcpy(vtable->version, AI_VERSION);
    return TRUE;
}